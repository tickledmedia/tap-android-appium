## IDE & Tools
1. IntelliJ IDEA 2021.3 (Community Edition)
2. Maven
3. Appium

---

## Building project

1. Open terminal
2. Use below command to build project jar file.

``mvn clean compile assembly:single``

3. Use below command to run project jar file.

``java -jar target/theasianparent-appium-sanity-1.0-SNAPSHOT-jar-with-dependencies.jar``

---