package libs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Command Prompt - this class contains method to run windows and mac commands
 */
public class CommandPrompt {

    Process p;
    ProcessBuilder builder;

    public static void main(String[] args) throws Exception {
        CommandPrompt cmd = new CommandPrompt();
        cmd.runCommand("dir");
    }

    /**
     * This method run command on windows and mac
     *
     * @param command to run
     */
    public String runCommand(String command) throws InterruptedException, IOException {
        String os = System.getProperty("os.name");
        //System.out.println(os);

        // build cmd proccess according to os
        if (os.contains("Windows")) { // if windows
            builder = new ProcessBuilder("cmd.exe", "/c", command);
            builder.redirectErrorStream(true);
            Thread.sleep(1000);
            p = builder.start();
        } else { // If Mac
            p = Runtime.getRuntime().exec(command);
        }
        // get std output
        BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line;
        StringBuilder allLine = new StringBuilder();
        int i = 1;
        while ((line = r.readLine()) != null) {
            System.out.println("while loop " + i + ". " + line);
            allLine.append(line).append("\n");
            if (line.contains("Console LogLevel: debug")) break;
            i++;
        }
        System.out.println("runCommand completed");
        return allLine.toString();
    }
}