import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import libs.BaseTest;
import org.openqa.selenium.Dimension;

import java.time.Duration;
import java.util.List;

public class oneGo extends BaseTest {

    static Environment environment = Environment.TEST;
    AppiumDriver<MobileElement> driver;

    public static void wait(int durationInMillis) {
        try {
            Thread.sleep(durationInMillis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        oneGo obj = new oneGo();
//        obj.setUp();
        obj.login();
        obj.navigation();
        obj.addCommunityPostNavigation();
        obj.articlesTabNavigation();
        obj.mediaTabNavigation();
        obj.pollsTabNavigation();
        obj.trackerNavigation();
        obj.appLogout();
        obj.closeApp();
//		obj.sendReport();
    }

//    public void setUp() {
//        DesiredCapabilities cap = new DesiredCapabilities();
//        cap.setCapability("platformName", "Android");
//        cap.setCapability("deviceName", "577372ad");
//        cap.setCapability("platformVersion", "11");
//        cap.setCapability("autoGrantPermissions", "true");
//        cap.setCapability("appPackage", "com.tickledmedia.ParentTown");
//        cap.setCapability("appActivity", "com.theasianparent.app.MainActivity");
//        System.out.println("*******Capabilities set************");
//        try {
//            driver = new AndroidDriver<MobileElement>(new URL("http://0.0.0.0:4723/wd/hub"), cap);
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        }
//        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//        SessionId sessionId = driver.getSessionId();
//        System.out.println("Session created. Session ID => " + sessionId);
//    }

    public void login() throws InterruptedException {

        Thread.sleep(500);
        findElementById("btnEmailSignUp").click();
        Thread.sleep(500);
//        driver.findElement(By.xpath(
//                        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup"))
//                .click();

        System.out.println("************Enter Parent-Child Frame*************");
        MobileElement txtEmail = driver.findElement(MobileBy.id("txtEmail"));
        txtEmail.clear();
        txtEmail.sendKeys("automationTest@gmail.com");
        System.out.println("Email keys sent");
        wait(2000);

        driver.findElement(MobileBy.id("txtPassword")).sendKeys("Automation@123");
        System.out.println("Password keys sent");
        Thread.sleep(500);
        driver.findElement(MobileBy.id("btnEmailSignUp")).click();
        Thread.sleep(1000);
    }

    public void navigation() throws InterruptedException {
//
        Thread.sleep(1000);
        driver.findElement(MobileBy.id("recycler_view")).click();

//		driver.findElement(MobileBy.id("toolbar")).findElement(MobileBy.className("android.widget.ImageButton")).click();
//		Thread.sleep(1000);
//
//		driver.navigate().back();


//		TouchAction a = new TouchAction(driver);
//		a.tap(84, 168).perform();
//		System.out.println("*************Profile button clicked**************");
//		Thread.sleep(1000);
//
//		TouchAction a2 = new TouchAction(driver);
//		a2.tap(84, 168).perform();
//
//		driver.findElement(By.id("img_notification")).click();
//		System.out.println("************Notification clicked*************");
//		Thread.sleep(1000);
//		TouchAction a3 = new TouchAction(driver);
//		a3.tap(84, 168).perform();
//
    }

    void mediaTabNavigation() throws InterruptedException {
        driver.findElement(MobileBy.id("navigation_media")).click();
        wait(5000);
    }

    void articlesTabNavigation() throws InterruptedException {
        driver.findElement(MobileBy.id("navigation_articles")).click();
        wait(5000);
        swipeScreen(Direction.LEFT);
        wait(1000);
        swipeScreen(Direction.LEFT);
        wait(1000);
        swipeScreen(Direction.LEFT);
        wait(1000);
        swipeScreen(Direction.LEFT);
        wait(1000);
        swipeScreen(Direction.LEFT);
        wait(1000);
    }

    void pollsTabNavigation() throws InterruptedException {
        driver.findElement(MobileBy.id("navigation_polls")).click();
        wait(5000);
        swipeScreen(Direction.UP);
        wait(1000);
        swipeScreen(Direction.UP);
        wait(1000);
        swipeScreen(Direction.UP);
        wait(1000);
        swipeScreen(Direction.UP);
        wait(1000);
        swipeScreen(Direction.UP);
        wait(1000);
        swipeScreen(Direction.UP);
    }

    void trackerNavigation() throws InterruptedException {

        driver.findElement(MobileBy.id("navigation_home")).click();
        wait(1000);
        driver.findElement(MobileBy.id("recycler_view")).click();
        wait(1000);

        List<MobileElement> imgTools = driver.findElementsById("img_tool");
        System.out.println("No of imgTools => " + imgTools.size());
        if (imgTools.size() > 2) {
            imgTools.get(1).click();
            wait(5000);
            navigateBack();
        }

        swipeScreen(Direction.UP);
        wait(1000);
        swipeScreen(Direction.UP);
        wait(1000);
        swipeScreen(Direction.UP);
        wait(1000);
        swipeScreen(Direction.UP);
        wait(1000);
        swipeScreen(Direction.UP);
        wait(1000);
        swipeScreen(Direction.UP);

        wait(5000);

        swipeScreen(Direction.DOWN);
        wait(1000);

        driver.findElement(MobileBy.id("lyt_add_pregnancy_kid")).click();
        driver.findElement(MobileBy.id("txt_edit")).click();
        wait(1000);
        driver.findElement(MobileBy.id("img_due_date_edit")).click();
        navigateBack();
        navigateBack();
    }

    public void addCommunityPostNavigation() {
        driver.findElement(MobileBy.id("fabAddPost")).click();
        wait(500);
        driver.findElement(MobileBy.id("lytPostQuestion")).click();

        driver.findElement(MobileBy.id("txt_title")).sendKeys("Boredom being Quarantine in 2020");
        wait(1000);

        driver.findElement(MobileBy.id("et_post")).sendKeys("Waiting for vacation");
//		WebDriverWait wait = new WebDriverWait(driver, 20);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("img_list_scroll")));

        driver.findElement(MobileBy.id("img_add")).click();

        wait(500);
        findElementById("iv_answer_image").click();
        findElementById("menu_photo").click();
        findElementById("lyt_nsfw").click();
        navigateBack();
        findElementById("button_secondary").click();

//		Thread.sleep(2000);
//		WebDriverWait wait1 = new WebDriverWait(driver, 20);
//		wait1.until(ExpectedConditions.visibilityOfElementLocated(By.id("emojiAppCompatTextView")));
//		System.out.println("***********Element frame visible*************");
//
//		driver.findElement(MobileBy.xpath(
//				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/androidx.cardview.widget.CardView[3]/android.view.ViewGroup/android.widget.RadioButton"))
//				.click();
//
//		System.out.println("**********Radio Button Clicked*************");
//		Thread.sleep(1000);
//
//		driver.findElement(MobileBy.id("btn_done")).click();
//		Thread.sleep(2000);

    }

    public void appLogout() {
        MobileElement profile = driver.findElement(MobileBy.id("toolbar")).findElement(MobileBy.className("android.widget.ImageButton"));
        profile.click();
        findElementById("action_settings").click();
        wait(500);
        findElementById("btn_logout").click();
        wait(500);
        findElementById("button_secondary").click();
        wait(2000);
    }

    public void closeApp() {
        driver.closeApp();
        System.out.println("*********Application Closed************");
    }

    MobileElement findElementById(String elementId) {
        return driver.findElement(MobileBy.id(elementId));
    }

    void navigateBack() {
        driver.navigate().back();
    }

    /**
     * Performs swipe from the center of screen
     *
     * @param dir the direction of swipe
     * @version java-client: 7.3.0
     **/
    public void swipeScreen(Direction dir) {
        if (environment == Environment.DEVELOPMENT) {
            System.out.println("Running script in development environment. Ignoring swipe.");
            return;
        }
        System.out.println("swipeScreen(): dir: '" + dir + "'"); // always log your actions

        // Animation default time:
        //  - Android: 300 ms
        //  - iOS: 200 ms
        // final value depends on your app and could be greater
        final int ANIMATION_TIME = 200; // ms

        final int PRESS_TIME = 200; // ms

        int edgeBorder = 10; // better avoid edges
        PointOption pointOptionStart, pointOptionEnd;

        // init screen variables
        Dimension dims = driver.manage().window().getSize();

        // init start point = center of screen
        pointOptionStart = PointOption.point(dims.width / 2, dims.height / 2);

        switch (dir) {
            case DOWN: // center of footer
                pointOptionEnd = PointOption.point(dims.width / 2, dims.height - edgeBorder);
                break;
            case UP: // center of header
                pointOptionEnd = PointOption.point(dims.width / 2, edgeBorder);
                break;
            case LEFT: // center of left side
                pointOptionEnd = PointOption.point(edgeBorder, dims.height / 2);
                break;
            case RIGHT: // center of right side
                pointOptionEnd = PointOption.point(dims.width - edgeBorder, dims.height / 2);
                break;
            default:
                throw new IllegalArgumentException("swipeScreen(): dir: '" + dir + "' NOT supported");
        }

        // execute swipe using TouchAction
        try {
            new TouchAction(driver)
                    .press(pointOptionStart)
                    // a bit more reliable when we add small wait
                    .waitAction(WaitOptions.waitOptions(Duration.ofMillis(PRESS_TIME)))
                    .moveTo(pointOptionEnd)
                    .release().perform();
        } catch (Exception e) {
            System.err.println("swipeScreen(): TouchAction FAILED\n" + e.getMessage());
            return;
        }

        // always allow swipe action to complete
        try {
            Thread.sleep(ANIMATION_TIME);
        } catch (InterruptedException e) {
            // ignore
        }
    }


}