import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import libs.BaseTest;

import java.io.File;
import java.util.List;

public class TAPApp extends BaseTest {

    public TAPApp() {
    }

    public TAPApp(int deviceNum) {
        super(deviceNum);
    }

    public static void main(String[] args) {
        // Create object
        TAPApp calc = new TAPApp();
        calc.execute();
    }

    public static void wait(int durationInMillis) {
        try {
            Thread.sleep(durationInMillis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void performOperations() {
        login();
        navigation();
        addCommunityPostNavigation();
        articlesTabNavigation();
        mediaTabNavigation();
        pollsTabNavigation();
        trackerNavigation();
        appLogout();
        closeApp();
//		sendReport();
    }

    public void login() {

        wait(500);
        findElementById("btnEmailSignUp").click();
        wait(500);
//        driver.findElement(By.xpath(
//                        "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup"))
//                .click();

        System.out.println("************Enter Parent-Child Frame*************");
        MobileElement txtEmail = driver.findElement(MobileBy.id("txtEmail"));
        txtEmail.clear();
        txtEmail.sendKeys("automationTest@gmail.com");
        System.out.println("Email keys sent");
        wait(2000);

        driver.findElement(MobileBy.id("txtPassword")).sendKeys("Automation@123");
        System.out.println("Password keys sent");
        wait(500);
        driver.findElement(MobileBy.id("btnEmailSignUp")).click();
        wait(1000);
    }

    public void navigation() {
//
        wait(1000);
        driver.findElement(MobileBy.id("recycler_view")).click();

//		driver.findElement(MobileBy.id("toolbar")).findElement(MobileBy.className("android.widget.ImageButton")).click();
//		Thread.sleep(1000);
//
//		driver.navigate().back();


//		TouchAction a = new TouchAction(driver);
//		a.tap(84, 168).perform();
//		System.out.println("*************Profile button clicked**************");
//		Thread.sleep(1000);
//
//		TouchAction a2 = new TouchAction(driver);
//		a2.tap(84, 168).perform();
//
//		driver.findElement(By.id("img_notification")).click();
//		System.out.println("************Notification clicked*************");
//		Thread.sleep(1000);
//		TouchAction a3 = new TouchAction(driver);
//		a3.tap(84, 168).perform();
//
    }

    void mediaTabNavigation() {
        driver.findElement(MobileBy.id("navigation_media")).click();
        wait(5000);
    }

    void articlesTabNavigation() {
        driver.findElement(MobileBy.id("navigation_articles")).click();
        wait(5000);
        Utils.swipeScreen(driver, Direction.LEFT);
        wait(1000);
        Utils.swipeScreen(driver, Direction.LEFT);
        wait(1000);
        Utils.swipeScreen(driver, Direction.LEFT);
        wait(1000);
        Utils.swipeScreen(driver, Direction.LEFT);
        wait(1000);
        Utils.swipeScreen(driver, Direction.LEFT);
        wait(1000);
    }

    void pollsTabNavigation() {
        driver.findElement(MobileBy.id("navigation_polls")).click();
        wait(5000);
        Utils.swipeScreen(driver, Direction.UP);
        wait(1000);
        Utils.swipeScreen(driver, Direction.UP);
        wait(1000);
        Utils.swipeScreen(driver, Direction.UP);
        wait(1000);
        Utils.swipeScreen(driver, Direction.UP);
        wait(1000);
        Utils.swipeScreen(driver, Direction.UP);
        wait(1000);
        Utils.swipeScreen(driver, Direction.UP);
    }

    void trackerNavigation() {

        driver.findElement(MobileBy.id("navigation_home")).click();
        wait(1000);
        driver.findElement(MobileBy.id("recycler_view")).click();
        wait(1000);

        List<MobileElement> imgTools = driver.findElementsById("img_tool");
        System.out.println("No of imgTools => " + imgTools.size());
        if (imgTools.size() > 2) {
            imgTools.get(1).click();
            wait(5000);
            navigateBack();
        }

        Utils.swipeScreen(driver, Direction.UP);
        wait(1000);
        Utils.swipeScreen(driver, Direction.UP);
        wait(1000);
        Utils.swipeScreen(driver, Direction.UP);
        wait(1000);
        Utils.swipeScreen(driver, Direction.UP);
        wait(1000);
        Utils.swipeScreen(driver, Direction.UP);
        wait(1000);
        Utils.swipeScreen(driver, Direction.UP);

        wait(5000);

        Utils.swipeScreen(driver, Direction.DOWN);
        wait(1000);

        driver.findElement(MobileBy.id("lyt_add_pregnancy_kid")).click();
        driver.findElement(MobileBy.id("txt_edit")).click();
        wait(1000);
        driver.findElement(MobileBy.id("img_due_date_edit")).click();
        navigateBack();
        navigateBack();
    }

    public void addCommunityPostNavigation() {
        driver.findElement(MobileBy.id("fabAddPost")).click();
        wait(500);
        driver.findElement(MobileBy.id("lytPostQuestion")).click();

        driver.findElement(MobileBy.id("txt_title")).sendKeys("Boredom being Quarantine in 2020");
        wait(1000);

        driver.findElement(MobileBy.id("et_post")).sendKeys("Waiting for vacation");
//		WebDriverWait wait = new WebDriverWait(driver, 20);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("img_list_scroll")));

        driver.findElement(MobileBy.id("img_add")).click();

        wait(500);
        findElementById("iv_answer_image").click();
        findElementById("menu_photo").click();
        findElementById("lyt_nsfw").click();
        navigateBack();
        findElementById("button_secondary").click();

//		Thread.sleep(2000);
//		WebDriverWait wait1 = new WebDriverWait(driver, 20);
//		wait1.until(ExpectedConditions.visibilityOfElementLocated(By.id("emojiAppCompatTextView")));
//		System.out.println("***********Element frame visible*************");
//
//		driver.findElement(MobileBy.xpath(
//				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/androidx.cardview.widget.CardView[3]/android.view.ViewGroup/android.widget.RadioButton"))
//				.click();
//
//		System.out.println("**********Radio Button Clicked*************");
//		Thread.sleep(1000);
//
//		driver.findElement(MobileBy.id("btn_done")).click();
//		Thread.sleep(2000);

    }

    public void appLogout() {
        MobileElement profile = driver.findElement(MobileBy.id("toolbar")).findElement(MobileBy.className("android.widget.ImageButton"));
        profile.click();
        findElementById("action_settings").click();
        wait(500);
        findElementById("btn_logout").click();
        wait(500);
        findElementById("button_secondary").click();
        wait(2000);
    }

    public void closeApp() {
        driver.closeApp();
        System.out.println("*********Application Closed************");
    }

    MobileElement findElementById(String elementId) {
        return driver.findElement(MobileBy.id(elementId));
    }

    void navigateBack() {
        driver.navigate().back();
    }

    public void run() {
        File app = new File("src/example/AndroidCalculator.apk");
        String appPath = app.getAbsolutePath();
        createDriver(appPath); // create devices
        performOperations(); // user function
    }

}