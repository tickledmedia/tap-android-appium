import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

public class testCase1 extends baseSetUp {

    @Test(priority = 1)
    public void invalidUserName() {
        MobileElement loginPage = (MobileElement) driver.findElement(By.id("txt_login"));
        loginPage.click();
        wait(500);

        MobileElement userNameTxtFld = (MobileElement) driver.findElement(By.id("et_user_email"));
        userNameTxtFld.clear();
        userNameTxtFld.sendKeys("invalid user email123333");

        MobileElement passwordTxtFld = (MobileElement) driver.findElement(By.id("et_password"));
        passwordTxtFld.sendKeys("jhsajdgastfwjrvksadb");

        MobileElement loginBtn = (MobileElement) driver.findElement(By.id("btn_login"));
        loginBtn.click();

        MobileElement errorTxt = (MobileElement) driver.findElement(By.id("textinput_error"));
        String actualErrorTxt = errorTxt.getAttribute("text");
        System.out.println("Actual Error Text - " + actualErrorTxt);
        String expectedErrorTxt = "Please enter valid email";

        Assert.assertEquals(actualErrorTxt, expectedErrorTxt);
    }

}
